## _Deprecation Notice_
This Pre-Built has been deprecated as of 09-01-2024 and will be end of life on 09-01-2025. The capabilities of this Pre-Built have been replaced by the [IP Fabric - REST](https://gitlab.com/itentialopensource/pre-built-automations/ip-fabric-rest)

<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your Pre-Built name -->
# IP Fabric Snapshot Inventory Comparison

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Requirements](#requirements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Additional Information](#additional-information)

## Overview

The IP Fabric Snapshot Inventory Comparison Pre-Built allows you to compare two network snapshots leveraging the [IP Fabric Automated Network Assurance Platform](https://ipfabric.io/product/). The Pre-Built has two stubbed tasks that enable the user to define if they would like to take the results of the comparison. 

For example, if there are new devices in the most recent snapshot, the user may decide to build a childJob that adds the device(s) to their inventory system such as SolarWinds, BlueCat, PRTG, Zabbix, Netbox, etc. If there are fewer devices in the most recent snapshot, the user may decide to build a childJob that removes the device(s) from their inventory system. 

_Estimated Run Time_: 1min

## Installation Prerequisites

Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2021.1`
  * [IP Fabric Adapter](https://gitlab.com/itentialopensource/adapters/telemetry-analytics/adapter-ipfabric)

## Requirements

This Pre-Built requires the following:

* [IP Fabric Adapter](https://gitlab.com/itentialopensource/adapters/telemetry-analytics/adapter-ipfabric) configured with credentials and IP Address of your IP Fabric Instance.

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button (as shown below).

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this Pre-Built can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the Pre-Built:

* Run the Operations Manager item `IP Fabric Snapshot Inventory Comparison` or call [IP Fabric Snapshot Inventory Comparison](./bundles/workflows/IP Fabric Snapshot Inventory Comparison) from your workflow as a child job.
